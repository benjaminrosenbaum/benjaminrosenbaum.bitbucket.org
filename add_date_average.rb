#! /usr/bin/env ruby

require 'date'

def datetime_sequence(start, final, step)
  dates = [start]
  while dates.last <= (final - step)
    dates << (dates.last + step)
  end 
  return dates
end 

times = Hash[`cut -d , -f 1,4 words.csv`
             .split("\n")
             .map do |line| 
                (k, v) = line.split(",")
                [ (k == "date") ? k : Date.parse(k), v.to_i * -1 ]
             end]


File.foreach ('words.csv') do |line|
  (date_str,words,avg,minutes) = line.chomp.split(",")
  #if (date_str == "date")
  #  puts "#{line.chomp},avg_minutes"
  #else
  if (date_str != "date")
    date = Date.parse(date_str)
    fortnight = datetime_sequence(date - 13, date, 1)
    fortnight_writing_time = fortnight.sum { |d| times[d] || 0 }
    avg_minutes = (fortnight_writing_time / 14.0).round
    puts "#{line.chomp},-#{avg_minutes}"
  end
end
    



