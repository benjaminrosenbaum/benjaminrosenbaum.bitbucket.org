
 
  // Darken a color
  function darkenColor(colorStr) {
    // Defined in dygraph-utils.js 
    var color = Dygraph.toRGB_(colorStr);
    color.r = Math.floor((255 + color.r) / 2);
    color.g = Math.floor((255 + color.g) / 2);
    color.b = Math.floor((255 + color.b) / 2);
    return 'rgb(' + color.r + ',' + color.g + ',' + color.b + ')';
  }
  
  function wordsBarChartPlotter(e) {
    barChartPlotter(e, 2.2, function(y){ return y;}, function(y, y_bottom){return y_bottom - y;})
  }

  function minutesBarChartPlotter(e) {
    barChartPlotter(e, 0.5, function(y){ return 0;}, function(y, y_bottom){return y;})
  }

  // This function draws bars for a single series.
  function barChartPlotter(e, widthFactor, yDeterminator, heightDeterminator) {
    var ctx = e.drawingContext;
    var points = e.points;
    var y_bottom = e.dygraph.toDomYCoord(0);

    ctx.fillStyle = darkenColor(e.color);

    // Find the minimum separation between x-values.
    // This determines the bar width.
    var min_sep = Infinity;
    for (var i = 1; i < points.length; i++) {
      var sep = points[i].canvasx - points[i - 1].canvasx;
      if (sep < min_sep) min_sep = sep;
    }
    var bar_width = Math.floor(2.0 / 3 * min_sep) * widthFactor; 

    // Do the actual plotting.
    for (var i = 0; i < points.length; i++) {
      var p = points[i];
      var center_x = p.canvasx;

      var x =  center_x - bar_width / 2;
      var y = yDeterminator(p.canvasy); 
      var w = bar_width;
      var h = heightDeterminator(p.canvasy, y_bottom)

      ctx.fillRect(x, y, w, h);
      ctx.strokeRect(x, y, w, h);
    }
  }

  function minutesTicker(min, max, pixels) {
                       return [
                         { v: -30 },
                         { label_v: -30, label: '30 minutes' },
                         { v: -60},
                         { label_v: -60, label: 'one hour' },
                         { v: -90 },
                         { label_v: -90, label: '1:30' },
                         { v: -120 },
                         { label_v: -120, label: 'two hours' },
                         { v: -150 },
                         { label_v: -150, label: '2:30' },
                         { v: -180 },
                         { label_v: -180, label: 'three hours' },
                         { v: -210 },
                         { label_v: -210, label: '3:30' }
                       ] 
  }

  function mainProject(timeSpentExpression) {
    result = timeSpentExpression;
    maxTime = 0;
    const parts = timeSpentExpression.split(";");
    for (const part of parts) {
      const match = part.match(/([a-z-]+)\((\d+)\)/);
      if (match) {
        const proj = match[1];
        const duration = match[2];
        if (duration > maxTime) {
          maxTime = duration;
          result = proj;
        }
      } 
    }

    return result;
  }

  function zoomAnnotations() {
    var showAnnotationsCheckbox = document.getElementById('showAnnotations');
    var showAnnotations = showAnnotationsCheckbox ? showAnnotationsCheckbox.checked : true;
    var showFullCheckbox = document.getElementById('showFull');
    var showFull = showFullCheckbox ? showFullCheckbox.checked : true;

    var annotationDivs = document.getElementsByClassName('dygraphDefaultAnnotation');
    for (var i = 0; i < annotationDivs.length; i++) {
      annotationDivs[i].style.width = '150px';
      annotationDivs[i].style.textAlign = 'left';
      annotationDivs[i].style.display = showAnnotations ? 'block' : 'none';
      if (projects) {
        var title = annotationDivs[i].innerText.replace(" ", "-").toLowerCase();

        const proj = mainProject(title);
        const v = projects[proj];
        //const [v] = Object.entries(projects).filter(e => title.includes(e[0])).map(e => e[1])
        annotationDivs[i].style.backgroundColor = v ?? 'white';
        if (showFull){
          annotationDivs[i].style.width = 'auto';
          annotationDivs[i].innerText = title;
        } else {
          annotationDivs[i].style.width = '8px';
          annotationDivs[i].style.height = '8px';
          annotationDivs[i].style.borderRadius = '8px';
          annotationDivs[i].innerText = ""
        }
      } 
    }

    timeLabels = [].slice.call(document.getElementsByClassName("dygraph-axis-label-y2"))
    timeLabels.forEach((e) => {
        //console.log(e)
        if (!e.dataset.cachedValue) {
          e.dataset.cachedValue = e.innerText
        }
        n = e.dataset.cachedValue * -1; 
        h = String(Math.floor(n/ 60)).padStart(2, '0'); 
        m = String(n % 60).padStart(2, '0');
        e.innerText = h + ":" + m
    });
  }

  document.getElementById("graphdiv").width = 1000;
  document.getElementById("graphdiv").height = 600;

  g = new Dygraph(

    // containing div
    document.getElementById("graphdiv"), "https://bitbucket.org/benjaminrosenbaum/benjaminrosenbaum.bitbucket.org/raw/master/words_final.csv",
    {
              labels: ['Date', 'Words', 'Avg', 'Minutes', 'Avg Min'],
              series: {
    //            "Total": {
    //              strokeWidth: 2
    //            },
    //            "Avg": {
    //              axis: "y1",
    //           },
                "Words": {
                  plotter: wordsBarChartPlotter
                },
                "Minutes": {
                  axis: "y2",
                  plotter: minutesBarChartPlotter,
                  ticker: minutesTicker
                },
                "Avg Min": {
                  axis: "y2",
                  color: "FF8C00",
                  strokeWidth: 2
                }
              },
              axes: {
                y:  { valueRange: [-500, 4000] },
                y2: { valueRange: [-900, 0] }    
              },
              independentTicks: 3, 
              colors: ["#007700", "#000077", "#770000" ],
              height: 700,
              width: 1400,
              legend: 'always',
              title: 'Daily writing progress',
              yLabel: 'Words',
              y2Label: 'Time Spent',
              
              showRangeSelector: true,
              //drawCallback: zoomAnnotations
              zoomCallback: zoomAnnotations,
              clickCallback: zoomAnnotations
    }
  );
  
  g.ready(function() { 
    g.setAnnotations(annotations); 

    var showAnnotationsCheckbox = document.getElementById('showAnnotations');
    showAnnotationsCheckbox.onchange = zoomAnnotations;
    var showFullCheckbox = document.getElementById('showFull');
    showFullCheckbox.onchange = zoomAnnotations;

    zoomAnnotations();

  });


