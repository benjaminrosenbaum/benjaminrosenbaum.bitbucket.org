

  annotations = [
    {
          series: "Avg",
          x: "2016-09-06",
          shortText: "game-writing->",
          text: "game-writing->",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2016-09-12",
          shortText: "<-game-writing",
          text: "<-game-writing",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2016-09-21",
          shortText: "tool-building",
          text: "tool-building",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2016-09-22",
          shortText: "world-building",
          text: "world-building",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-01-28",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-02-17",
          shortText: "USA->",
          text: "USA->",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-03-15",
          shortText: "<-USA",
          text: "<-USA",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-04-17",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-04-20",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-04-21",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-04-23",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-04-27",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-04-28",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-05-02",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-05-03",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-05-04",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-05-10",
          shortText: "USA->",
          text: "USA->",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-06-07",
          shortText: "<-USA",
          text: "<-USA",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-06-19",
          shortText: "Soraya",
          text: "Soraya",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-07-02",
          shortText: "USA->",
          text: "USA->",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-08-06",
          shortText: "<-USA",
          text: "<-USA",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-08-20",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-09-04",
          shortText: "<-u7.read",
          text: "<-u7.read",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-09-11",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-10-01",
          shortText: "Unrv.ms->Piper",
          text: "Unrv.ms->Piper",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-10-06",
          shortText: "TheReturn.ms->VD18",
          text: "TheReturn.ms->VD18",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-11-09",
          shortText: "USA->",
          text: "USA->",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-05",
          shortText: "<-USA",
          text: "<-USA",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-10",
          shortText: "dream-apart-0.3",
          text: "dream-apart-0.3",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-11",
          shortText: "dream-apart-0.3",
          text: "dream-apart-0.3",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-12",
          shortText: "dream-apart-0.3",
          text: "dream-apart-0.3",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-13",
          shortText: "dream-apart-0.3",
          text: "dream-apart-0.3",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-14",
          shortText: "dream-apart-0.3",
          text: "dream-apart-0.3",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2017-12-16",
          shortText: "TheReturn->",
          text: "TheReturn->",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-01-06",
          shortText: "belter-game",
          text: "belter-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-01-07",
          shortText: "dream-apart-0.3.1",
          text: "dream-apart-0.3.1",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-01-08",
          shortText: "dream-apart-0.3.1",
          text: "dream-apart-0.3.1",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-01-12",
          shortText: "belter-game",
          text: "belter-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-01-15",
          shortText: "siri-on-artemisia",
          text: "siri-on-artemisia",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-03-04",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-03-05",
          shortText: "die-aufloesung-readthrough",
          text: "die-aufloesung-readthrough",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-03-10",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-04-10",
          shortText: "belter-game",
          text: "belter-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-04-10",
          shortText: "belter-game",
          text: "belter-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-07-03",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-08-16",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-09-22",
          shortText: "finished Reef Six",
          text: "finished Reef Six",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-09-23",
          shortText: "shtetl-world",
          text: "shtetl-world",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-09-29",
          shortText: "shtetl-world",
          text: "shtetl-world",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-10-22",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-10-29",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-20",
          shortText: "choice-of-games",
          text: "choice-of-games",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-23",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-24",
          shortText: "owlhack",
          text: "owlhack",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-26",
          shortText: "owlhack",
          text: "owlhack",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-28",
          shortText: "owlhack",
          text: "owlhack",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-29",
          shortText: "owlhack",
          text: "owlhack",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-11-30",
          shortText: "owlhack",
          text: "owlhack",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-01",
          shortText: "choice-of-games",
          text: "choice-of-games",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-04",
          shortText: "siri on artemisia",
          text: "siri on artemisia",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-07",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-11",
          shortText: "choice-of-games",
          text: "choice-of-games",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-17",
          shortText: "choice-of-games",
          text: "choice-of-games",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-18",
          shortText: "molly2",
          text: "molly2",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-20",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-21",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-22",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-23",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-25",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-26",
          shortText: "siri-on-artemisia",
          text: "siri-on-artemisia",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2018-12-27",
          shortText: "reef-six-ccf",
          text: "reef-six-ccf",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-28",
          shortText: "spring-in-the-shtetl(75);reef-six",
          text: "spring-in-the-shtetl(75);reef-six",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-30",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-01-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-01",
          shortText: "bereft",
          text: "bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-03",
          shortText: "bereft",
          text: "bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-04",
          shortText: "meeting-minutes",
          text: "meeting-minutes",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-06",
          shortText: "salutations",
          text: "salutations",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-07",
          shortText: "reef-six-networks",
          text: "reef-six-networks",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-20",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-21",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-22",
          shortText: "liz-game",
          text: "liz-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-24",
          shortText: "spring-in-the-shtetl(136);liz-game",
          text: "spring-in-the-shtetl(136);liz-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-02-27",
          shortText: "rejoice",
          text: "rejoice",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-01",
          shortText: "liz-game",
          text: "liz-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-02",
          shortText: "liz-game",
          text: "liz-game",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-05",
          shortText: "liz-game(64);morrigan(43)",
          text: "liz-game(64);morrigan(43)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-07",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-11",
          shortText: "rejoice",
          text: "rejoice",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-12",
          shortText: "rejoice",
          text: "rejoice",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-13",
          shortText: "rejoice",
          text: "rejoice",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-19",
          shortText: "rejoice",
          text: "rejoice",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-20",
          shortText: "rejoice(49);wiwi(11);spring-in-the-shtetl(24)",
          text: "rejoice(49);wiwi(11);spring-in-the-shtetl(24)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-21",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-22",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-03-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-04-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-05-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-06",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-07",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-08",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-09",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-11",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-12",
          shortText: "maghds-tale(86);syho(34)",
          text: "maghds-tale(86);syho(34)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-14",
          shortText: "rejoice",
          text: "rejoice",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-15",
          shortText: "bereft",
          text: "bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-17",
          shortText: "bereft",
          text: "bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-18",
          shortText: "bereft",
          text: "bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-19",
          shortText: "bereft(114);wiwi(41)",
          text: "bereft(114);wiwi(41)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-20",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-28",
          shortText: "bereft",
          text: "bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-06-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-07-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-08-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-04",
          shortText: "MH2-dybbuk",
          text: "MH2-dybbuk",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-05",
          shortText: "MH2-dybbuk(100);spring-in-the-shtetl(54)",
          text: "MH2-dybbuk(100);spring-in-the-shtetl(54)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-06",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-08",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-12",
          shortText: "spring-in-the-shtetl(91);wiwi(84)",
          text: "spring-in-the-shtetl(91);wiwi(84)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-13",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-14",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-15",
          shortText: "wiwi(115);spring-in-the-shtetl(19)",
          text: "wiwi(115);spring-in-the-shtetl(19)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-09-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-01",
          shortText: "spring-in-the-shtetl(90);the-dybbuk(60)",
          text: "spring-in-the-shtetl(90);the-dybbuk(60)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-05",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-06",
          shortText: "belters",
          text: "belters",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-07",
          shortText: "belters",
          text: "belters",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-08",
          shortText: "belters",
          text: "belters",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-10",
          shortText: "belters",
          text: "belters",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-12",
          shortText: "spring-in-the-shtetl(146);belters(177)",
          text: "spring-in-the-shtetl(146);belters(177)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-13",
          shortText: "wiwi(87);belters(199)",
          text: "wiwi(87);belters(199)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-16",
          shortText: "spring-in-the-shtetl(76);morrigan(18)",
          text: "spring-in-the-shtetl(76);morrigan(18)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-18",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-19",
          shortText: "wiwi(140);MH2-dybbuk(87)",
          text: "wiwi(140);MH2-dybbuk(87)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-20",
          shortText: "wiwi",
          text: "wiwi",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-10-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-15",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-16",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-17",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-18",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-19",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-21",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-22",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-23",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-24",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-27",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-28",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-29",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-11-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-05",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-06",
          shortText: "danny(75);spring-in-the-shtetl(15)",
          text: "danny(75);spring-in-the-shtetl(15)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-07",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-07",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2019-12-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-22",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-23",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-24",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-25",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-26",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-29",
          shortText: "the-boys",
          text: "the-boys",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-01-31",
          shortText: "the-boys",
          text: "the-boys",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-06",
          shortText: "the-unraveling;bereft",
          text: "the-unraveling;bereft",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-07",
          shortText: "spring-in-the-shtetl(89);the-unraveling(16)",
          text: "spring-in-the-shtetl(89);the-unraveling(16)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-10",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-11",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-13",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-14",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-16",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-17",
          shortText: "the-unraveling(50);spring-in-the-shtetl(44)",
          text: "the-unraveling(50);spring-in-the-shtetl(44)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-20",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-21",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-23",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-24",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-25",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-27",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-02-28",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-05",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-06",
          shortText: "the-unraveling(70);spring-in-the-shtetl(54)",
          text: "the-unraveling(70);spring-in-the-shtetl(54)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-08",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-25",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-27",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-03-31",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-04-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-28",
          shortText: "the-boys",
          text: "the-boys",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-05-29",
          shortText: "maghds-tale",
          text: "maghds-tale",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-09",
          shortText: "the-boys",
          text: "the-boys",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-11",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-12",
          shortText: "brainstorming",
          text: "brainstorming",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-16",
          shortText: "the-boys;spring-in-the-shtetl",
          text: "the-boys;spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-06-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-26",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-30",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-07-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-08-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-12",
          shortText: "the-middle-bridge",
          text: "the-middle-bridge",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-20",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-21",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-22",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-23",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-24",
          shortText: "57(spring-in-the-shtetl);161(she-ra)",
          text: "57(spring-in-the-shtetl);161(she-ra)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-25",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-26",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-27",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-29",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-09-30",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-01",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-07",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-09",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-10",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-15",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-16",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-19",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-24",
          shortText: "she-ra(66);spring-in-the-shtetl(36)",
          text: "she-ra(66);spring-in-the-shtetl(36)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-30",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-10-31",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-01",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-02",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-07",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-11",
          shortText: "she-ra;the-unraveling",
          text: "she-ra;the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-12",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-13",
          shortText: "she-ra;the-unraveling",
          text: "she-ra;the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-14",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-16",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-17",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-18",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-20",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-21",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-22",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-23",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-24",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-25",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-26",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-27",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-28",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-11-30",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-01",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-02",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-04",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-06",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-07",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-08",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-11",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-12",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-13",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-14",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-15",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-16",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-17",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-18",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-23",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-26",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-27",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-29",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2020-12-30",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-04",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-05",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-10",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-11",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-12",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-16",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-17",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-19",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-21",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-25",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-26",
          shortText: "she-ra",
          text: "she-ra",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-01-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-02-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-17",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-03-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-01",
          shortText: "the-unraveling",
          text: "the-unraveling",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-03",
          shortText: "wander-dreaming",
          text: "wander-dreaming",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-04",
          shortText: "wander-dreaming",
          text: "wander-dreaming",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-06",
          shortText: "wander-dreaming(39);spring-in-the-shtetl(97)",
          text: "wander-dreaming(39);spring-in-the-shtetl(97)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-21",
          shortText: "game-design(63);spring-in-the-shtetl",
          text: "game-design(63);spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-22",
          shortText: "game-design",
          text: "game-design",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-04-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-22",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-29",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-05-30",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-03",
          shortText: "droplet-2.0",
          text: "droplet-2.0",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-04",
          shortText: "droplet-2.0",
          text: "droplet-2.0",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-05",
          shortText: "droplet-2.0",
          text: "droplet-2.0",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-06-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-06",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-07",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-08",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-09",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-12",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-13",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-16",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-19",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-20",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-21",
          shortText: "spring-in-the-shtetl(88);promo-essays",
          text: "spring-in-the-shtetl(88);promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-22",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-23",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-25",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-07-28",
          shortText: "promo-essays",
          text: "promo-essays",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-11",
          shortText: "tgwwfm-film",
          text: "tgwwfm-film",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-08-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-05",
          shortText: "tgwwfm-film",
          text: "tgwwfm-film",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-24",
          shortText: "birdsense",
          text: "birdsense",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-25",
          shortText: "spring-in-the-shtetl;birdsense",
          text: "spring-in-the-shtetl;birdsense",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-09-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-02",
          shortText: "birdsense",
          text: "birdsense",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-10-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-02",
          shortText: "birdsense",
          text: "birdsense",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-04",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-05",
          shortText: "sargasso",
          text: "sargasso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-24",
          shortText: "spring-in-the-shtetl;sargassso",
          text: "spring-in-the-shtetl;sargassso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-26",
          shortText: "spring-in-the-shtetl;sargassso",
          text: "spring-in-the-shtetl;sargassso",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-11-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-09",
          shortText: "spring-in-the-shtetl;etc.",
          text: "spring-in-the-shtetl;etc.",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-12",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-13",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-14",
          shortText: "danny;motherless men",
          text: "danny;motherless men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-15",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-16",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-19",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-21",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2021-12-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-18",
          shortText: "essay",
          text: "essay",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-21",
          shortText: "birdsense",
          text: "birdsense",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-01-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-02-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-03-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-11",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-12",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-13",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-22",
          shortText: "tgwwfm-film",
          text: "tgwwfm-film",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-23",
          shortText: "tgwwfm-film",
          text: "tgwwfm-film",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-25",
          shortText: "tgwwfm-film",
          text: "tgwwfm-film",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-04-30",
          shortText: "sensitivity-read",
          text: "sensitivity-read",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-01",
          shortText: "sensitivity-read",
          text: "sensitivity-read",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-02",
          shortText: "sensitivity-read",
          text: "sensitivity-read",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-03",
          shortText: "sensitivity-read",
          text: "sensitivity-read",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-05-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-01",
          shortText: "tgwwfm",
          text: "tgwwfm",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-11",
          shortText: "danny",
          text: "danny",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-06-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-07-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-08-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-17",
          shortText: "spring-in-the-shtetl(79);credo(34)",
          text: "spring-in-the-shtetl(79);credo(34)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-09-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-29",
          shortText: "motherless-men",
          text: "motherless-men",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-10-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-02",
          shortText: "motherless-men(55);reef-six(55)",
          text: "motherless-men(55);reef-six(55)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-11-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2022-12-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-01-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-02-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-03-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-04-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-05-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-06-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-07-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-27",
          shortText: "rosenstrasse-essay",
          text: "rosenstrasse-essay",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-08-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-28",
          shortText: "spring-in-the-shtetl(76);numinous(69)",
          text: "spring-in-the-shtetl(76);numinous(69)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-09-30",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-03",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-05",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-06",
          shortText: "spring-in-the-shtetl(141);numinous(52)",
          text: "spring-in-the-shtetl(141);numinous(52)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-07",
          shortText: "spring-in-the-shtetl(27);numinous(126)",
          text: "spring-in-the-shtetl(27);numinous(126)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-10",
          shortText: "spring-in-the-shtetl(87);numinous(40)",
          text: "spring-in-the-shtetl(87);numinous(40)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-14",
          shortText: "spring-in-the-shtetl(120);numinous(37)",
          text: "spring-in-the-shtetl(120);numinous(37)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-16",
          shortText: "spring-in-the-shtetl(62);numinous(28)",
          text: "spring-in-the-shtetl(62);numinous(28)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-21",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-22",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-23",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-27",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-28",
          shortText: "morrigan(66);numinous(114)",
          text: "morrigan(66);numinous(114)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-29",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-10-30",
          shortText: "morrigan(14);numinous(33);spring-in-the-shtetl(77)",
          text: "morrigan(14);numinous(33);spring-in-the-shtetl(77)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-03",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-04",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-07",
          shortText: "spring-in-the-shtetl(90);numinous(35)",
          text: "spring-in-the-shtetl(90);numinous(35)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-11-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2023-12-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-05",
          shortText: "spring-in-the-shtetl(62);numinous(150)",
          text: "spring-in-the-shtetl(62);numinous(150)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-07",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-08",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-09",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-10",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-14",
          shortText: "spring-in-the-shtetl(30);numinous(55)",
          text: "spring-in-the-shtetl(30);numinous(55)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-15",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-21",
          shortText: "spring-in-the-shtetl(75);numinous(114)",
          text: "spring-in-the-shtetl(75);numinous(114)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-22",
          shortText: "numinous(130);spring-in-the-shtetl(12)",
          text: "numinous(130);spring-in-the-shtetl(12)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-24",
          shortText: "numinous(81);spring-in-the-shtetl(78)",
          text: "numinous(81);spring-in-the-shtetl(78)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-26",
          shortText: "spring-in-the-shtetl(52);numinous(25)",
          text: "spring-in-the-shtetl(52);numinous(25)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-28",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-01-31",
          shortText: "numinous(132);spring-in-the-shtetl(61)",
          text: "numinous(132);spring-in-the-shtetl(61)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-01",
          shortText: "numinous(254);spring-in-the-shtetl(51)",
          text: "numinous(254);spring-in-the-shtetl(51)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-02",
          shortText: "spring-in-the-shtetl(111);numinous(69)",
          text: "spring-in-the-shtetl(111);numinous(69)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-03",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-04",
          shortText: "spring-in-the-shtetl(98);numinous(92)",
          text: "spring-in-the-shtetl(98);numinous(92)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-08",
          shortText: "spring-in-the-shtetl(75);numinous(16)",
          text: "spring-in-the-shtetl(75);numinous(16)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-10",
          shortText: "numinous(192);spring-in-the-shtetl(54)",
          text: "numinous(192);spring-in-the-shtetl(54)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-10",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-12",
          shortText: "numinous(156);spring-in-the-shtetl(84)",
          text: "numinous(156);spring-in-the-shtetl(84)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-13",
          shortText: "numinous(152);spring-in-the-shtetl(93)",
          text: "numinous(152);spring-in-the-shtetl(93)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-14",
          shortText: "spring-in-the-shtetl(184);numinous(41)",
          text: "spring-in-the-shtetl(184);numinous(41)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-17",
          shortText: "numinous(54);spring-in-the-shtetl(36)",
          text: "numinous(54);spring-in-the-shtetl(36)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-19",
          shortText: "spring-in-the-shtetl(241);numinous(14)",
          text: "spring-in-the-shtetl(241);numinous(14)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-21",
          shortText: "spring-in-the-shtetl(162);gospel-of-barabbas(26)",
          text: "spring-in-the-shtetl(162);gospel-of-barabbas(26)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-22",
          shortText: "spring-in-the-shtetl(124);gospel-of-barabbas(10)",
          text: "spring-in-the-shtetl(124);gospel-of-barabbas(10)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-23",
          shortText: "numinous(82);spring-in-the-shtetl(71)",
          text: "numinous(82);spring-in-the-shtetl(71)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-28",
          shortText: "spring-in-the-shtetl(152);numinous(29)",
          text: "spring-in-the-shtetl(152);numinous(29)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-02-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-01",
          shortText: "numinous(84);spring-in-the-shtetl(65)",
          text: "numinous(84);spring-in-the-shtetl(65)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-03-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-04-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-14",
          shortText: "spring-in-the-shtetl(224);tools(99);numinous(90)",
          text: "spring-in-the-shtetl(224);tools(99);numinous(90)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-22",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-24",
          shortText: "spring-in-the-shtetl(100);numinous(37)",
          text: "spring-in-the-shtetl(100);numinous(37)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-30",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-05-31",
          shortText: "ted-talk(386);numinous(44)",
          text: "ted-talk(386);numinous(44)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-01",
          shortText: "essays(tedcon)",
          text: "essays(tedcon)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-07",
          shortText: "essays(tedcon)",
          text: "essays(tedcon)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-21",
          shortText: "spring-in-the-shtetl(166);numinous(139)",
          text: "spring-in-the-shtetl(166);numinous(139)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-23",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-28",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-29",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-06-30",
          shortText: "numinous(30);spring-in-the-shtetl(13)",
          text: "numinous(30);spring-in-the-shtetl(13)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-01",
          shortText: "spring-in-the-shtetl(62);numinous(25)",
          text: "spring-in-the-shtetl(62);numinous(25)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-03",
          shortText: "spring-in-the-shtetl(56);numinous(20)",
          text: "spring-in-the-shtetl(56);numinous(20)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-04",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-05",
          shortText: "numinous(70);spring-in-the-shtetl(56)",
          text: "numinous(70);spring-in-the-shtetl(56)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-09",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-10",
          shortText: "spring-in-the-shtetl(186);numinous(110)",
          text: "spring-in-the-shtetl(186);numinous(110)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-13",
          shortText: "numinous(41)",
          text: "numinous(41)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-18",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-19",
          shortText: "numinous(184);spring-in-the-shtetl(120)",
          text: "numinous(184);spring-in-the-shtetl(120)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-20",
          shortText: "spring-in-the-shtetl(102);numinous(85)",
          text: "spring-in-the-shtetl(102);numinous(85)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-23",
          shortText: "spring-in-the-shtetl(108);numinous(45)",
          text: "spring-in-the-shtetl(108);numinous(45)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-24",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-25",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-29",
          shortText: "spring-in-the-shtetl(255);numinous(54)",
          text: "spring-in-the-shtetl(255);numinous(54)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-07-31",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-03",
          shortText: "spring-in-the-shtetl(70);numinous(10)",
          text: "spring-in-the-shtetl(70);numinous(10)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-04",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-05",
          shortText: "numinous(279);spring-in-the-shtetl(89)",
          text: "numinous(279);spring-in-the-shtetl(89)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-06",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-07",
          shortText: "spring-in-the-shtetl(34);numinous(29)",
          text: "spring-in-the-shtetl(34);numinous(29)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-08",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-11",
          shortText: "spring-in-the-shtetl(373);numinous(23)",
          text: "spring-in-the-shtetl(373);numinous(23)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-15",
          shortText: "spring-in-the-shtetl(128);numinous(39)",
          text: "spring-in-the-shtetl(128);numinous(39)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-21",
          shortText: "spring-in-the-shtetl(161);numinous(96)",
          text: "spring-in-the-shtetl(161);numinous(96)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-23",
          shortText: "spring-in-the-shtetl(264);numinous(60)",
          text: "spring-in-the-shtetl(264);numinous(60)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-24",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-26",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-27",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-28",
          shortText: "spring-in-the-shtetl(105);numinous(72)",
          text: "spring-in-the-shtetl(105);numinous(72)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-29",
          shortText: "numinous(186);spring-in-the-shtetl(67)",
          text: "numinous(186);spring-in-the-shtetl(67)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-08-30",
          shortText: "spring-in-the-shtetl(82);numinous(80)",
          text: "spring-in-the-shtetl(82);numinous(80)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-01",
          shortText: "numinous(109);spring-in-the-shtetl(60)",
          text: "numinous(109);spring-in-the-shtetl(60)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-04",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-06",
          shortText: "numinous(101);spring-in-the-shtetl(15)",
          text: "numinous(101);spring-in-the-shtetl(15)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-07",
          shortText: "numinous(121);spring-in-the-shtetl(105)",
          text: "numinous(121);spring-in-the-shtetl(105)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-08",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-10",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-11",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-12",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-13",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-14",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-15",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-16",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-17",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-18",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-19",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-20",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-21",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-22",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-23",
          shortText: "spring-in-the-shtetl(221);numinous(59)",
          text: "spring-in-the-shtetl(221);numinous(59)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-24",
          shortText: "spring-in-the-shtetl(137);numinous(80)",
          text: "spring-in-the-shtetl(137);numinous(80)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-25",
          shortText: "spring-in-the-shtetl(185);cog-ww(29)",
          text: "spring-in-the-shtetl(185);cog-ww(29)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-26",
          shortText: "spring-in-the-shtetl(194);cog-ww(40)",
          text: "spring-in-the-shtetl(194);cog-ww(40)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-27",
          shortText: "cog-vmp(148);spring-in-the-shtetl(120)",
          text: "cog-vmp(148);spring-in-the-shtetl(120)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-29",
          shortText: "cog-vmp(257);numinous(8)",
          text: "cog-vmp(257);numinous(8)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-29",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-09-30",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-01",
          shortText: "cog-vmp(80);numinous(43);spring-in-the-shtetl(20)",
          text: "cog-vmp(80);numinous(43);spring-in-the-shtetl(20)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-02",
          shortText: "housekeeping(67);gg-novel(46)",
          text: "housekeeping(67);gg-novel(46)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-04",
          shortText: "gg-novel(64);cog-vmp(23)",
          text: "gg-novel(64);cog-vmp(23)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-06",
          shortText: "gg-novel(122);cog-vmp(50)",
          text: "gg-novel(122);cog-vmp(50)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-07",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-08",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-10",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-13",
          shortText: "cog-vmp(70);numinous(10)",
          text: "cog-vmp(70);numinous(10)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-14",
          shortText: "cog-vmp(146);numinous(43)",
          text: "cog-vmp(146);numinous(43)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-15",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-16",
          shortText: "cog-vmp(132);morrigan(40);cog-ww(2)",
          text: "cog-vmp(132);morrigan(40);cog-ww(2)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-17",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-18",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-19",
          shortText: "morrigan",
          text: "morrigan",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-20",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-21",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-22",
          shortText: "gg-novel(88);numinous(37);cog-vmp(17)",
          text: "gg-novel(88);numinous(37);cog-vmp(17)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-23",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-24",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-25",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-26",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-27",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-28",
          shortText: "numinous(223);cog-vmp-research(48)",
          text: "numinous(223);cog-vmp-research(48)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-29",
          shortText: "numinous(112);spring-in-the-shtetl(11)",
          text: "numinous(112);spring-in-the-shtetl(11)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-10-30",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-01",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-02",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-03",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-04",
          shortText: "cog-vmp(54);spring-in-the-shtetl(22)",
          text: "cog-vmp(54);spring-in-the-shtetl(22)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-05",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-07",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-08",
          shortText: "spring-in-the-shtetl(55);numinous(23)",
          text: "spring-in-the-shtetl(55);numinous(23)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-09",
          shortText: "spring-in-the-shtetl",
          text: "spring-in-the-shtetl",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-10",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-11",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-13",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-14",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-15",
          shortText: "cog-vmp-research(23);numinous(23)",
          text: "cog-vmp-research(23);numinous(23)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-17",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-18",
          shortText: "cog-vmp(87);cog-vmp-research(25)",
          text: "cog-vmp(87);cog-vmp-research(25)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-19",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-20",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-21",
          shortText: "numinous(86);cog-vmp(15);gg-novel(6)",
          text: "numinous(86);cog-vmp(15);gg-novel(6)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-25",
          shortText: "numinous(115);gg-novel(78)",
          text: "numinous(115);gg-novel(78)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-26",
          shortText: "gg-novel(95);cog-vmp(45)",
          text: "gg-novel(95);cog-vmp(45)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-28",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-11-29",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-01",
          shortText: "cog-vmp(189);numinous(5)",
          text: "cog-vmp(189);numinous(5)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-02",
          shortText: "numinous(203);cog-vmp(45)",
          text: "numinous(203);cog-vmp(45)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-03",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-04",
          shortText: "numinous(84);cog-vmp(40)",
          text: "numinous(84);cog-vmp(40)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-09",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-10",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-11",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-12",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-15",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-16",
          shortText: "memeostasis",
          text: "memeostasis",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-18",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-20",
          shortText: "dream-apart",
          text: "dream-apart",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-20",
          shortText: "memeostasis(116);gg-novel(40)",
          text: "memeostasis(116);gg-novel(40)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-23",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-26",
          shortText: "numinous(55);cog-vmp(76)",
          text: "numinous(55);cog-vmp(76)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-27",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-28",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-29",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-30",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2024-12-31",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-01",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-022",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-04",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-07",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-08",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-09",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-10",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-13",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-14",
          shortText: "cog-vmp(68);numinous(47);cog-vmp-research(16)",
          text: "cog-vmp(68);numinous(47);cog-vmp-research(16)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-15",
          shortText: "cog-vmp(188);numinous(84)",
          text: "cog-vmp(188);numinous(84)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-16",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-17",
          shortText: "cog-vmp(188);numinous(84)",
          text: "cog-vmp(188);numinous(84)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-18",
          shortText: "numinous(6);cog-vmp(6)",
          text: "numinous(6);cog-vmp(6)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-19",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-21",
          shortText: "dream-apart(142);numinous(20)",
          text: "dream-apart(142);numinous(20)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-22",
          shortText: "cog-vmp(144);numinous(89);dream-apart(10)",
          text: "cog-vmp(144);numinous(89);dream-apart(10)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-23",
          shortText: "numinous(102);tools(88)",
          text: "numinous(102);tools(88)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-24",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-25",
          shortText: "numinous(71);cog-vmp(61);tools(52)",
          text: "numinous(71);cog-vmp(61);tools(52)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-26",
          shortText: "cog-vmp(93);numinous(30)",
          text: "cog-vmp(93);numinous(30)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-27",
          shortText: "cog-vmp(107);numinous(93)",
          text: "cog-vmp(107);numinous(93)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-29",
          shortText: "tools(263);numinous(64)",
          text: "tools(263);numinous(64)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-29",
          shortText: "cog-vmp(136);numinous(75);tools(16)",
          text: "cog-vmp(136);numinous(75);tools(16)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-30",
          shortText: "cog-vmp(189);numinous(156)",
          text: "cog-vmp(189);numinous(156)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-01-31",
          shortText: "cog-vmp-research(180); cog-vmp(4)",
          text: "cog-vmp-research(180); cog-vmp(4)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-01",
          shortText: "cog-vmp(122);numinous(90);tools(35)",
          text: "cog-vmp(122);numinous(90);tools(35)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-02",
          shortText: "cog-vmp(173);numinous(16)",
          text: "cog-vmp(173);numinous(16)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-03",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-04",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-05",
          shortText: "numinous",
          text: "numinous",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-06",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-07",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-08",
          shortText: "cog-ww(354);cog-vmp(25)",
          text: "cog-ww(354);cog-vmp(25)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-09",
          shortText: "cog-vmp(159);cog-ww(31)",
          text: "cog-vmp(159);cog-ww(31)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-10",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-11",
          shortText: "cog-vmp(215);cog-vmp-research(10)",
          text: "cog-vmp(215);cog-vmp-research(10)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-12",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-14",
          shortText: "cog-vmp-research",
          text: "cog-vmp-research",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-17",
          shortText: "numinous(99);cog-vmp(26)",
          text: "numinous(99);cog-vmp(26)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-18",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-19",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-20",
          shortText: "cog-vmp(102);gg-novel(76)",
          text: "cog-vmp(102);gg-novel(76)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-21",
          shortText: "gg-novel",
          text: "gg-novel",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-22",
          shortText: "cog-ww(114);cog-vmp-research(52)",
          text: "cog-ww(114);cog-vmp-research(52)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-23",
          shortText: "gg-novel(125);numinous(8)",
          text: "gg-novel(125);numinous(8)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-24",
          shortText: "the-fledgling(100);cog-vmp(51);tools(10)",
          text: "the-fledgling(100);cog-vmp(51);tools(10)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-25",
          shortText: "cog-vmp",
          text: "cog-vmp",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-26",
          shortText: "cog-vmp(132);numinous(15)",
          text: "cog-vmp(132);numinous(15)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-27",
          shortText: "cog-vmp(75);cog-ww(64);cog-vmp-research(28)",
          text: "cog-vmp(75);cog-ww(64);cog-vmp-research(28)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-02-28",
          shortText: "cog-vmp(185);numinous(13);cog-ww(9)",
          text: "cog-vmp(185);numinous(13);cog-ww(9)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-03-01",
          shortText: "cog-vmp(124);cog-ww(12);numinous(3)",
          text: "cog-vmp(124);cog-ww(12);numinous(3)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-03-02",
          shortText: "cog-ww(114);numinous(97)",
          text: "cog-ww(114);numinous(97)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-03-03",
          shortText: "numinous(71);cog-vmp(61);cog-vmp-research(40);cog-ww(33)",
          text: "numinous(71);cog-vmp(61);cog-vmp-research(40);cog-ww(33)",
          cssClass: "rotated"
      },
    {
          series: "Avg",
          x: "2025-03-04",
          shortText: "cog-ww",
          text: "cog-ww",
          cssClass: "rotated"
      }]

  ytd_piechart_data =  [
     ['Project', 'Hours'], 
       
     ['cog-vmp', 64.61666666666666],
     ['numinous', 45.45],
     ['cog-ww', 14.816666666666666],
     ['gg-novel', 8.816666666666666],
     ['cog-vmp-research', 7.45],
     ['tools', 3.35],
     ['dream-apart', 2.533333333333333],
     ['the-fledgling', 1.6666666666666667],
     [' cog-vmp', 0.06666666666666667]
  ]

  ytd_words_data = [
     ['Project', 'Words'], 
       
     ['The Fledgling', 2186
],
     ['ghost & golem novel', 6444
],
     ['Regarding the Childhood of Morrigan', 366
],
     ['Fatimid Cairo by Night', 9813
]
  ]
  

  last3wks_piechart_data =  [
     ['Project', 'Hours'], 
       
     ['cog-vmp', 24.083333333333332],
     ['cog-ww', 8.4],
     ['gg-novel', 5.683333333333334],
     ['numinous', 5.1],
     ['cog-vmp-research', 4.016666666666667],
     ['the-fledgling', 1.6666666666666667],
     ['tools', 0.16666666666666666]
  ]

  last3wks_words_data = [
     ['Project', 'Words'], 
       
     ['The Fledgling', 2186
],
     ['ghost & golem novel', 6444
],
     ['Regarding the Childhood of Morrigan', 366
],
     ['Fatimid Cairo by Night', 2790
]
  ]
  

  last6mos_piechart_data =  [
     ['Project', 'Hours'], 
       
     ['cog-vmp', 120.53333333333333],
     ['numinous', 76.5],
     ['spring-in-the-shtetl', 66.05],
     ['gg-novel', 34.016666666666666],
     ['cog-ww', 16.0],
     ['cog-vmp-research', 9.866666666666667],
     ['memeostasis', 4.116666666666666],
     ['dream-apart', 4.05],
     ['morrigan', 3.4833333333333334],
     ['tools', 3.35],
     ['the-fledgling', 1.6666666666666667],
     ['housekeeping', 1.1166666666666667],
     [' cog-vmp', 0.06666666666666667]
  ]

  last6mos_words_data = [
     ['Project', 'Words'], 
       
     ['The Fledgling', 2186
],
     ['ghost & golem novel', 19431
],
     ['Memeostasis', 1025
],
     ['Regarding the Childhood of Morrigan', 11397
],
     ['Spring in the shtetl', 7358
],
     ['Fatimid Cairo by Night', 13375
]
  ]
  

  all_piechart_data =  [
     ['Project', 'Hours'], 
       
     ['spring-in-the-shtetl', 2015.2666666666667],
     ['numinous', 187.26666666666668],
     ['the-unraveling', 132.43333333333334],
     ['cog-vmp', 120.53333333333333],
     ['she-ra', 79.13333333333334],
     ['gg-novel', 34.016666666666666],
     ['maghds-tale', 23.783333333333335],
     ['wiwi', 23.7],
     ['promo-essays', 23.383333333333333],
     ['morrigan', 21.1],
     ['belters', 17.85],
     ['cog-ww', 16.0],
     ['sensitivity-read', 15.25],
     ['dream-apart-0.3', 15.25],
     ['bereft', 13.216666666666667],
     ['dream-apart', 12.666666666666666],
     ['motherless-men', 11.416666666666666],
     ['sargasso', 10.433333333333334],
     ['danny', 10.333333333333334],
     ['cog-vmp-research', 9.866666666666667],
     ['owlhack', 8.7],
     ['rejoice', 8.45],
     ['tgwwfm-film', 6.5],
     ['ted-talk', 6.433333333333334],
     ['MH2-dybbuk', 6.333333333333333],
     ['wander-dreaming', 5.833333333333333],
     ['belter-game', 5.583333333333333],
     ['choice-of-games', 5.083333333333333],
     ['tools', 5.0],
     ['dream-apart-0.3.1', 4.683333333333334],
     ['rosenstrasse-essay', 4.266666666666667],
     ['memeostasis', 4.116666666666666],
     ['shtetl-world', 3.9],
     ['liz-game', 3.85],
     ['the-boys', 3.7],
     ['siri-on-artemisia', 3.3666666666666667],
     ['meeting-minutes', 3.1666666666666665],
     ['birdsense', 3.033333333333333],
     ['droplet-2.0', 2.4],
     ['game-design', 2.1333333333333333],
     ['die-aufloesung-readthrough', 2.1166666666666667],
     ['Soraya', 1.6833333333333333],
     ['the-fledgling', 1.6666666666666667],
     ['salutations', 1.4333333333333333],
     ['reef-six-ccf', 1.3166666666666667],
     ['brainstorming', 1.2166666666666666],
     ['essay', 1.1666666666666667],
     ['housekeeping', 1.1166666666666667],
     ['tgwwfm', 1.0],
     ['the-dybbuk', 1.0],
     ['reef-six', 0.9166666666666666],
     ['molly2', 0.75],
     ['gospel-of-barabbas', 0.6],
     ['reef-six-networks', 0.5833333333333334],
     ['credo', 0.5666666666666667],
     ['syho', 0.5666666666666667],
     ['the-middle-bridge', 0.4166666666666667],
     [' cog-vmp', 0.06666666666666667]
  ]

  as_of = 'Wed Mar  5 13:32:55 CET 2025'

