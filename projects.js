// order matters: if you worked on multiple projects on a given day, the last
// one listed here will determine the color of the annotation
var projects = {

	"reef-six": "#ffd000",
	"salutations": "#ffee88",
	"meeting-minutes": "#ffee88",
	"bereft": "#ffee00",
	"unraveling": "#ffaa00",
	"wiwi": "#edd000",
	"die-aufloesung-readthrough": "#ffaa00",
	"droplet-2.0": "#dcc000",	

	"soraya": "#ddddff",
	"sargasso": "#ddddff",

	"maghds-tale": "#ff55ff",
	
	"danny": "#ffffff",
	"siri": "#ffffff",
	"molly2": "#ffffff",
	"morrigan": "#ffffff",
	"motherless men": "#ffffff",
	"owlhack": "#ffffff",
	"the-boys": "#ffffff",
	"rejoice": "#ffffff",

	"choice-of-games": "#99ffaa",
	"spring-in-the-shtetl": "#99ffaa",
	"cog-vmp": "#99e9ee",
	"cog-vmp-research": "#99e9ff",
	"cog-ww": "#88e8ee",
	"gg-novel": "#60ffaa",
	
	"belter-game": "#88aaff",
	"belters": "#88aaff",
	"MH2-dybbuk": "#88aaff",
	"dream-apart": "#88aaff",
	"liz-game": "#88aaff",
	"shtetl-world": "#88aaff",
	"numinous": "#88aadd",
	
	"tools": "#cccccc",
	"tool-building": "#cccccc",
	"world-building": "#cccccc",
}

